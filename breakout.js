var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

var x = canvas.width / 2;
var y = canvas.height - 30;

var dx = 2;
var dy = -2;
var ballColor = '#0095DD';
var ballRadius = 10;

var paddleColor = '#0095DD';
var paddleHeight = 10;
var paddleWidth = 75;
var paddleX = (canvas.width-paddleWidth)/2;
var paddleY = 10;

var rightPressed = false;
var leftPressed = false;

var bricks;
var brickRowCount = 3;
var brickColumnCount = 5;
var brickWidth = 75;
var brickHeight = 20;
var brickPadding = 10;
var brickOffsetTop = 30;
var brickOffsetLeft = 30;

var score = 0;
var scoreColor = '#0095DD';
var lives = 3;
var gameRunning = false;

activate();

function activate() {
    buildBricks();
    setEventListeners();
    draw();
}

function buildBricks() {
    bricks = [];

    for(var c=0; c<brickColumnCount; c++) {
        bricks[c] = [];
        for(var r=0; r<brickRowCount; r++) {
            bricks[c][r] = { x: 0, y: 0, status: 1 };
        }
    }
}

function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function detectBrickCollision() {
    for(var c = 0; c<brickColumnCount; c++) {
        for(var r = 0; r<brickRowCount; r++) {
            var b = bricks[c][r];

            if (b.status) {
                if (x > b.x && x < b.x + brickWidth && y > b.y && y < b.y + brickHeight) {
                    onBrickHit(b);

                    if(score == brickRowCount * brickColumnCount) {
                        onWin();
                    }
                }
            }
        }
    }
}

function detectCollision() {
    detectPaddleCollision();
    detectEdgeCollision();
    detectBrickCollision();
}

function detectEdgeCollision() {
    // Reverse x direction
    if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) {
        dx = -dx;
    }

    // Reverse y direction
    if (y + dy < ballRadius) {
        dy = -dy;
    }

    else if (y + dy > canvas.height - ballRadius) {
        lives--;

        if (lives) {
            resetPaddle();
            resetBall();
            pauseGame();
        } else {
            endGame();
        }
    }
}

function detectPaddleCollision() {

    if (x + dx > paddleX && x + dx < paddleX + paddleWidth) {
        if (y + dy + ballRadius < canvas.height - paddleY && y + dy + ballRadius > canvas.height - paddleY - paddleHeight) {
            dy = -dy;
        }
    }
}

function draw() {
    if (gameRunning) {
        clearCanvas();
        drawBricks();
        detectCollision();
        drawBall();
        drawPaddle();
        moveBall();
        movePaddle();
        drawScore();
        drawLives();
    } else {
        clearCanvas();
        drawBricks();
        drawBall();
        drawPaddle();
        drawScore();
        drawLives();
        drawPauseMessage();
    }

    requestAnimationFrame(draw);
}

function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI * 2);
    ctx.fillStyle = ballColor;
    ctx.fill();
    ctx.closePath();
}

function drawBricks() {
    for(var c = 0; c<brickColumnCount; c++) {
        for(var r = 0; r<brickRowCount; r++) {
            var brick = bricks[c][r];

            if (brick.status) {
                var brickX = (c * (brickWidth + brickPadding)) + brickOffsetLeft;
                var brickY = (r * (brickHeight + brickPadding)) + brickOffsetTop;

                brick.x = brickX;
                brick.y = brickY;
                ctx.beginPath();
                ctx.rect(brickX, brickY, brickWidth, brickHeight);
                ctx.fillStyle = "#0095DD";
                ctx.fill();
                ctx.closePath();
            }
        }
    }
}

function drawLives() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#0095DD";
    ctx.fillText("Lives: "+lives, canvas.width-65, 20);
}

function drawPaddle() {
    ctx.beginPath();
    ctx.rect(paddleX, canvas.height - paddleHeight - paddleY, paddleWidth, paddleHeight);
    ctx.fillStyle = paddleColor;
    ctx.fill();
    ctx.closePath();
}

function drawPauseMessage() {
    ctx.font = '52px Arial';
    ctx.fillStyle = scoreColor;
    ctx.fillText('PAUSED', 135, 170);
}

function drawScore() {
    ctx.font = '16px Arial';
    ctx.fillStyle = scoreColor;
    ctx.fillText('Score: ' + score, 8, 20);
}

function endGame() {
    dx = 0;
    dy = 0;
    alert("GAME OVER");
    document.location.reload();
}

function keyDownHandler(e) {
    if (e.keyCode == 39) {
        rightPressed = true;
    }
    else if (e.keyCode == 37) {
        leftPressed = true;
    }
}

function keyUpHandler(e) {
    if (e.keyCode == 39) {
        rightPressed = false;
    } else if (e.keyCode == 37) {
        leftPressed = false;
    } else if (e.keyCode == 32) {
        gameRunning = !gameRunning;
    }
}

function mouseMoveHandler(e) {
    var relativeX = e.clientX - canvas.offsetLeft;

    if(gameRunning && relativeX > 0 && relativeX < canvas.width) {
        paddleX = relativeX - paddleWidth/2;
    }
}

function moveBall() {
    x += dx;
    y += dy;
}

function movePaddle() {
    if (rightPressed && paddleX < canvas.width - paddleWidth) {
        paddleX += 7;
    } else if (leftPressed && paddleX > 0) {
        paddleX -= 7;
    }
}

function onBrickHit(brick) {
    dy = -dy;
    brick.status = 0;
    score++;
}

function onWin() {
    alert("YOU WIN, CONGRATULATIONS!");
    document.location.reload();
}

function resetBall() {
    x = canvas.width/2;
    y = canvas.height-30;
    dx = 2;
    dy = -2;
}

function resetPaddle() {
    paddleX = (canvas.width-paddleWidth)/2;
}

function setEventListeners() {
    document.addEventListener('keydown', keyDownHandler, false);
    document.addEventListener('keyup', keyUpHandler, false);
    document.addEventListener('mousemove', mouseMoveHandler, false);
}

function pauseGame() {
    gameRunning = false;
}
